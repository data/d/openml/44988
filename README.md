# OpenML dataset: QSAR_TID_11

https://www.openml.org/d/44988

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

This dataset contains drugs and their potency.
The features of the drug are FCFP 1024bit Molecular Fingerprints which were generated from SMILES strings. They were obtained using the Pipeline Pilot program, Dassault Systemes BIOVIA.

**Attribute Description**

1. *MEDIAN_PXC50* - target feature
2. *MOLECULE_CHEMBL_ID* - id
3. *FCFP.* - molecular fingerprints

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44988) of an [OpenML dataset](https://www.openml.org/d/44988). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44988/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44988/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44988/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

